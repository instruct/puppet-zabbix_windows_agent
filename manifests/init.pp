class zabbix_windows_agent (
   String $server,
   String $serveractive,
   String $listenport,
   String $logfile,
   Integer $logfilesize,
   Integer $debuglevel,
   Integer $enableremotecommands,
   Integer $logremotecommands,
   String $hostname = $::trusted['certname'],
) {

  package { 'zabbix-agent':
    ensure   => installed,
    provider => 'chocolatey',
  }

  file { 'c:/programdata/zabbix/zabbix_agentd.conf':
    ensure  => present,
    mode    => '0644',
    notify  => Service['Zabbix Agent'],
    require => Package['zabbix-agent'],
    content => template('zabbix_windows_agent/zabbix_agentd.conf.erb'),
  }
  file { 'c:/zabbix':
    ensure  => directory,
    require => Package['zabbix-agent'],
  }

  file { 'c:/programdata/zabbix/zabbix_agentd.d':
    ensure  => directory,
    require => Package['zabbix-agent'],
  }

  service { 'Zabbix Agent':
    ensure     => running,
    enable     => true,
    hasstatus  => true,
    hasrestart => true,
  }

}
