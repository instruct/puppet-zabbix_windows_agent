define zabbix_windows_agent::userparameters (
  String $content,
) {
  file { "c:/programdata/zabbix/zabbix_agentd.d/${name}.conf":
    ensure  => present,
    content => $content,
    notify  => Service['Zabbix Agent'],
  }
}
